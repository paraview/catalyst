/*
 * Distributed under OSI-approved BSD 3-Clause License. See
 * accompanying License.txt
 */

#ifndef catalyst_conduit_h
#define catalyst_conduit_h

// Include the external `conduit.h`.
#include <conduit.h>

// Include the symbols catalyst guarantees to provide.
#include "catalyst_conduit_abi.h"

#endif
