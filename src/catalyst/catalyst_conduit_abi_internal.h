/*
 * Distributed under OSI-approved BSD 3-Clause License. See
 * accompanying License.txt
 */

#ifndef catalyst_conduit_abi_h
#define catalyst_conduit_abi_h

// Include the internal `conduit.h`.
#include "conduit.h"

#endif
