Catalyst
########

This document refers to the Catalyst API which was first introduced in
ParaView 5.9. For earlier versions of Catalyst, please refer to earlier `docs`_.

.. _docs: https://www.paraview.org/files/catalyst/docs/ParaViewCatalystUsersGuide_v2.pdf

.. toctree::
   :caption: Contents:
   :titlesonly:

   introduction
   build_and_install
   for_simulation_developers
   for_implementation_developers
   catalyst_replay
   debugging
